# REDDIT ROBOT
a quick and dirty telegram bot written by Jad Pamment

### WHAT DOES IT DO? 
It posts five posts from a specified subreddit to whichever group chat you've added this bot too it simply posts urls a text title and is completely type agnostic. 
For those of you who don't know - [telegram](https://telegram.org/) is an encrypted chat platform like signal or wickr. [reddit](https://www.reddit.com/) is a replacement for digg and stumbleupon that has progressively gotten worse and worse over the years. It has an API and no default filtering which is why I'm using it here. 

### HOW DOES IT WORK? 
Using existing wrapper libraries [praw](https://praw.readthedocs.io/en/latest/) and [telegram](https://praw.readthedocs.io/en/latest/). 
Telegram server calls the telegram wrapper functions in this client, which in turn call praw functions to pull 5 posts from reddit, returning as list of urls simply posted to chat. Telegram handles the rest. 

### HOW CAN I RUN IT?
Local Setup:
* setup a virtual env and acivate (actually probably do that before cloning)
* using pip, install telegram and praw
* contact @botFather and create a bot in telegram 
* settup an user & app in reddit to obtain reddit client secret, id and user agent
* run `python reddit_robot.py` from the src dir
* ????
* add your telegram to your group chat
* happy shitposting

Alternatively just use the dockerfile and run with a mounted volume containing valid token configs 
`docker run -d -v /source:/home/tel_bot/localConf --name tel_bot_five_1 pammenjk/tel_bot:0.1`


### TROUBLESHOOTING
Q: Jad, the python interpreter is displaying errors when I run this code. What do I do? 

A: Look at the errors being thrown and then either browse stack overflow or just fix them. Honestly the python interpreter is in the same category as cargo for useful feedback. 

Q: Jad why is there no requirements.txt included? 

A: Because I couldn't be bothered with venv with I was developing this?

### DEPLOY
No one wants to have to leave a programming running in the foreground on their dev machine, so eventually your going to have to deploy this somewhere. 
* If you have a AWS or AZURE ansible or terraform may come in handy. 
* If you have a spare linux box floating around - use that, Rasberry Pi might be ideal. 
* Note: if you do happen to build an ansible or terraform script - feel free to put through a PR

### FINAL NOTES
No I did not write unit tests, I hacked this together over five beers on a Saturday night when a couple of friends were in my house eating chips. There are loads of improvements to make, some of which I'll probably implement:
* TRUE and HONEST logging
* a basic pytest suite
* a db for caching 'top' results rather than making GET calls all the type
* the inclusion of actual deployment tools/scripts
* post type filtering

If you've got any feedback, or changes checkout the code and create a PR. 