FROM ubuntu

RUN apt-get update -y
RUN DEBIAN_FRONTEND="noninteractive" TZ="Pacific/Auckland" apt-get -y install tzdata
RUN apt-get install python3 nano python3-pip python3-lxml -y
RUN python3 -m pip install --upgrade pip setuptools
WORKDIR /home/tel_bot/
RUN mkdir src
RUN mkdir logs
RUN mkdir localConf
COPY src/* src/
RUN pip install wheel
COPY requirements.txt .
RUN pip install -r requirements.txt
CMD python3 src/reddit_robot.py