import praw
from prawcore.exceptions import Forbidden
from urllib.parse import urlsplit, urljoin, urlunparse
from enum import Enum
import os
import html_utils


class MessageType(Enum):
    IMG = 0
    VID = 1
    TXT = 2


def get_top_five(subreddit_args, rating, reddit_log, user_id, secret, agent_in):
    reddit_log.info(" Calling Reddit....")
    details = {}
    reddit = praw.Reddit(
        client_id=user_id, client_secret=secret, user_agent=agent_in)
    post_limit = 5
    try:
        # Inital subreddit handling done here
        reddit_log.info(" Getting the {} posts {}".format(
            str(rating), str(subreddit_args)))
        sub_reddit = return_sub(rating, reddit, subreddit_args, reddit_log)
        details = get_posts(sub_reddit, post_limit, reddit_log)
    except:
        reddit_log.error(
            " Sorry - that subreddit {} does not exist.".format(str(subreddit_args)))
        # Add in search
    return details


def return_sub(rating, reddit, subreddit_args, reddit_log):
    # return subreddit contents based on ratings and subreddit name
    rate = str(rating)
    sub_reddit = ""
    try:
        if rate == "hot":
            sub_reddit = reddit.subreddit(str(subreddit_args)).hot()
        elif rate == "new":
            sub_reddit = reddit.subreddit(str(subreddit_args)).new()
        elif rate == "cont":
            sub_reddit = reddit.subreddit(str(subreddit_args)).controversial()
        else:
            sub_reddit = reddit.subreddit(str(subreddit_args)).top()
    except Forbidden:
        reddit_log.info("Looks like we're getting 403's")
    return sub_reddit


def get_posts(sub_reddit, post_limit, reddit_log):
    # Get five posts from a subreddit and ignore stickied post
    count = 0
    output_posts = {}
    for submission in sub_reddit:
        if submission.stickied == False:
            output_posts[str(submission.title)] = str(submission.url)
            count += 1
        if count >= post_limit:
            break
    reddit_log.info(" Have Retrieved 5 posts....")
    return output_posts


def sorting_parameter(args, current_subreddit, reddit_log, sub_list):
    if len(args) == 0:
        reddit_log.info(
            " Getting current subreddit {}".format(current_subreddit))
        user_message = sub_list[current_subreddit]
        reddit_log.info("List len: {}".format((len(sub_list) - 1)))
        reddit_log.info("Getting {} from {} using {}".format(
            user_message, sub_list, current_subreddit))
        if int(current_subreddit) >= int((len(sub_list) - 1)):
            current_subreddit = 0
        else:
            current_subreddit += 1
        parameter = "hot"
    elif len(args) == 2:
        user_message = ''.join(args[0])
        parameter = ''.join(args[1])
    else:
        user_message = ''.join(args[0])
        parameter = 'hot'
    return user_message, parameter, current_subreddit


def get_all_posts(subreddits, update, user_message, parameter, logging):
    if len(subreddits) == 0:
        update.message.reply_text(
            "We haven't got anything for {}. It's either non-existent, deleted or in quarantine. Please try again.".format(user_message))
    else:
        update.message.reply_text(
            "Here are the {} five posts for {}:\n".format(parameter, user_message))
        # Add in handler for urls
        for (k, v) in subreddits.items():
            url_dict = url_scrubber(v, logging)
            print(url_dict)
            for (link, type) in url_dict.items():
                message_generator(k, link, type, update, logging)


def url_scrubber(url, logging):
    output = urlsplit(url)
    file_type = os.path.splitext(str(output.path))[1]
    url_out = {}

    if not file_type:
        # Return urls as list length >= 1
        url_list, type = extract_links(url, output)
        if type is MessageType.TXT:
            url_out[url_list] = type
        else:
            for i in url_list:
                url_out[i] = type

    else:
        logging.info(f"File type:{file_type}")
        if str(output.path).endswith(".gifv") or str(output.path).endswith(".gif"):
            out_path = output.path.replace(file_type, ".mp4")
            url_out[str(output.scheme+"://"+output.netloc+out_path)
                    ] = MessageType.VID
        elif str(output.path).endswith(".jpg") or str(output.path).endswith(".png"):
            url_out[str(url)] = MessageType.IMG

    return url_out

# Dear sweet jesus refactor this pile of shit.
def extract_links(url, url_parts):
    return_type = MessageType.TXT
    domain = str(url_parts.netloc)
    if "redgifs.com" in domain:
        new_url = html_utils.redgifs_video_handler(url)
        return_type = MessageType.VID
    elif "erome.com" in domain:
        new_url = html_utils.ero_album_handler(url)
        for li in new_url:
            if "mp4" in str(li):
                return_type = MessageType.VID
            else:
                return_type = MessageType.IMG
    elif "gfycat.com" in domain or "gifdeliverynetwork" in domain:
        output = urlsplit(html_utils.gfycat_video_handler(url)[0])
        out_path = output.path.replace(".mp4", "-mobile.mp4")
        new_url = [str(output.scheme+"://"+output.netloc+out_path)]
        return_type = MessageType.VID
    else:
        new_url = url
        return_type = MessageType.TXT

    return new_url, return_type


def message_generator(title, url_out, message_type, update, logging):
    logging.info("Returning type {}".format(message_type))
    if message_type == MessageType.TXT:
        sub_response = ''.join("\n{}:\n{}".format(title, url_out))
        update.message.reply_text('{}'.format(sub_response))
        print(f"TEXT MESSAGE: {update}")
    elif message_type == MessageType.IMG:
        update.message.reply_photo(photo=url_out, caption=title+": "+url_out)
        print(f"IMG MESSAGE: {update}")
    elif message_type == MessageType.VID:
        update.message.reply_video(video=url_out, caption=title+": "+url_out)
        print(f"VID MESSAGE {update}")
