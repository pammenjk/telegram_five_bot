import datetime
import logging
import logging.handlers
import os
import configparser


def logging_initalizer(local_dirname):
    log_file_name = "logs/{}_reddit.log".format(
        datetime.datetime.now().strftime("%Y%m%d%H%M%S"))
    logging_filename = os.path.join(local_dirname, log_file_name)

    open(logging_filename, 'w+')
    logging.basicConfig(filename=logging_filename, level=logging.INFO)
    logging_init = logging.getLogger()
    return logging_init


def configuration_loader(reddit_log, default_conf_path, local_dirname):
    reddit_log.info(" loading up configuration details")
    main_path = ""
    if os.path.exists(default_conf_path):
        main_path = default_conf_path
    else:
        main_path = local_dirname

    config_filename = os.path.join(main_path, "config.ini")
    data_filename = os.path.join(main_path, "data.ini")
    # Define config settings first
    config = configparser.ConfigParser()
    return data_filename, config_filename, config


def configuration_initalizer(config_filename, data_filename, config, reddit_log):
    # read and setup config files
    try:
        config.read_file(open(config_filename))
        config.read_file(open(data_filename))
        reddit_log.info(" Config is initialized....")
    except:
        reddit_log.error(" Config initialization has failed....")

        local_token = ""
        local_client_id = ""
        local_client_secret = ""
        local_user_agent = ""
        local_sub_list = ""

    # Define token values for connecting to both services
    try:
        reddit_log.info(" Setting up token values....")
        local_token = str(config['TELEGRAM']['telegram_token'])
        local_client_id = str(config['REDDIT']['my_client_id'])
        local_client_secret = str(config['REDDIT']['my_client_secret'])
        local_user_agent = str(config['REDDIT']['my_user_agent'])
        local_sub_list = list(str(config['SUBS']['sub_list']).split(','))
    except KeyError as e:
        reddit_log.error(" Config Key Error Encountered {}".format(e))

    return local_user_agent, local_client_secret, local_client_id, local_sub_list, local_token
