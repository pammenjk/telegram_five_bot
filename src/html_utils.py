import requests
from bs4 import BeautifulSoup

# Handler for erome.com albums - returns a list of urls


def ero_album_handler(input_url):
    links_list = []

    page = requests.get(input_url)
    soup = BeautifulSoup(page.text, features='lxml')

    for l in soup.find_all('source'):
        links_list.append(l.get('src'))

    return links_list


def redgifs_video_handler(input_url):
    links_list = []

    page = requests.get(input_url)
    soup = BeautifulSoup(page.text, features='lxml')

    for el in soup.find_all('source'):
        vid_link = el.get('src')
        if "mobile" in str(vid_link):
            links_list = [vid_link]
    return links_list


def gfycat_video_handler(input_url):
    links_list = []

    page = requests.get(input_url)
    soup = BeautifulSoup(page.text, features='lxml')

    for el in soup.find_all('source'):
        vid_link = el.get('src')
        if ".mp4" in str(vid_link):
            links_list = [vid_link]
    return links_list
