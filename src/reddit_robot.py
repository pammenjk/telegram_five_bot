#!/usr/bin/env python3
from telegram.ext import Updater, CommandHandler
from telegram.error import (TelegramError, Unauthorized, BadRequest,
                            TimedOut, ChatMigrated, NetworkError)
import requests
import json
import praw
import configparser
import os
import datetime
import logging
import logging.handlers
from pathlib import Path
import utils
import request_handler

# Global Variables
local_dirname = os.path.abspath(
    os.path.join(os.path.dirname(__file__), os.pardir))

default_conf_path = os.path.abspath(
    os.path.join("/home/tel_bot/", "localConf"))
current_subreddit = 0
telegram_token = ""
client_id = ""
reddit_token = ""
agent_in = ""
logging = 0


def start(update, context):
    # Start the app

    logging.info("Calling Start....")
    update.message.reply_text('Hi {}, welcome to reddit_five_bot. Type help to get a more info'.format(
        update.message.from_user.first_name))


def helper(update, context):
    # Return a help message
    logging.info("Calling Help....")
    update.message.reply_text(
        'help - give me a subreddit or don\'t. Syntax is: /get_five <subreddit> <rating - hot, new, cont, top>')

# Return five posts based on sort args and subreddit name


def get_five(update, context):
    logging.info(" Calling Get Five....")
    global current_subreddit
    logging.info(" {} arguments have been provided...".format(
        str(len(context.args))))
    try:
        # Bulk of the work is done in the below three functions - sort the requests
        user_message, parameter, current_subreddit = request_handler.sorting_parameter(
            context.args, current_subreddit, logging, sub_list)
    # Get top five urls from subreddit
        subreddits = request_handler.get_top_five(user_message,
                                                  parameter, logging, client_id, reddit_token, agent_in)
        # Return the posts
        request_handler.get_all_posts(
            subreddits, update, user_message, parameter, logging)
    except TypeError:
        logging.info("Could find the correct type")


# Manditory error handler

def error_handler(update, context):
    try:
        raise context.error
    except Unauthorized:
        # remove update.message.chat_id from conversation list
        print("Unauthorised sorry please check authorisation")
    except BadRequest as e:
        # handle malformed requests - read more below!
        print(f"{update}")
        print(f"Request is malformed: {e}")
    except TimedOut:
        # handle slow connection problems
        print("Connection has timed out")

    except NetworkError:
        # handle other connection problems
        print("Network appears to be broken. Don't look at me.")

    except ChatMigrated:
        # the chat_id of a group has changed, use e.new_chat_id instead
        print("The ID of the group you're connecting to has changed.")

    except TelegramError:
        # handle all other telegram related errors
        print("Yeah, I have no idea either")


def robot_loop():

    tele_dispatch = Updater(telegram_token, use_context=True)

    tele_dispatch.dispatcher.add_handler(CommandHandler('start', start))
    tele_dispatch.dispatcher.add_handler(CommandHandler('help', helper))
    tele_dispatch.dispatcher.add_handler(
        CommandHandler('get_five', get_five, pass_args=True))
    tele_dispatch.dispatcher.add_error_handler(error_handler)
    # Spinning it up
    try:
        logging.info(" Reddit Robot spinning up....")
        tele_dispatch.start_polling()
        tele_dispatch.idle()
    except Exception:
        logging.error(" Reddit Robot startup has failed....")


# Initialise logging and configuration data
logging = utils.logging_initalizer(local_dirname)
data_filename, config_filename, config = utils.configuration_loader(
    logging, default_conf_path, local_dirname)
agent_in, reddit_token, client_id, sub_list, telegram_token = utils.configuration_initalizer(
    config_filename, data_filename, config, logging)
# Start robot application
robot_loop()
